# Red Ant Comics

This app will comprise of a listing page of comics with the ability to add comics to a favourites list.

## Usage

Install modules to app

```
npm install
```

Run the app in the development mode in [http://localhost:3000](http://localhost:3000).

```
npm start
```

Builds the app for production to the `build` folder.

```
npm run build
```

## Technology

* ReactJS
* Redux
* BootStrap

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Thanks!
