import './style/normalize.css';
import './style/main.css';
import './style/custom.css';

import ComicList from './components/ComicList';

function App() {
  return (
    <div className="App">
  		<ComicList />
    </div>
  );
}

export default App;
