import * as ActionTypes from '../constants/ActionTypes';

export function fetchComics() {
  return dispatch => {
    dispatch({
      type: ActionTypes.FETCH_COMICS_REQUEST,
    })

    fetch("https://gateway.marvel.com/v1/public/comics?apikey=3cb62d086d5debdeea139095cbb07fe4&ts=redant&hash=140e85a50884cef76d614f6dacada288")
      .then(res => res.json())
      .then(json => {
        dispatch({
          type: ActionTypes.FETCH_COMICS_SUCCESS,
          payload: json.data.results
        })
        return json.data.results;
      })
      .catch(error => dispatch({
        type: ActionTypes.FETCH_COMICS_FAILURE,
        payload: error
      }));
  };
}

export function addFavComic(payload) {
  return {
    type: ActionTypes.ADD_FAV_COMIC,
    payload
  }
};

export function removeFavComic(payload) {
  return {
    type: ActionTypes.REMOVE_FAV_COMIC,
    payload
  }
};
