import React from 'react';
import PropTypes from 'prop-types';

const Comic = (props) => {
	const comic = props.comic;
	return (
		<div>
			<img
				src={comic.images.length > 0 ? `${comic.images[0].path}/portrait_uncanny.${comic.images[0].extension}` : `https://via.placeholder.com/300x450`}
				role="presentation"
				className="rounded"
				alt={comic.login}
			/>
			<h2>{comic.title}</h2>
		</div>
	);
};

Comic.propTypes = {
	comic: PropTypes.object,
};

export default Comic;
