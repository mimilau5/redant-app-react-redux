import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faStar} from '@fortawesome/free-solid-svg-icons'

import {fetchComics, addFavComic, removeFavComic} from "../actions";
import Loading from '../components/Loading';
import Comic from '../components/Comic';
import FavouritePanel from '../components/FavouritePanel';

import _filter from 'lodash/filter';
import _forEach from 'lodash/forEach';
import _find from 'lodash/find';

class ComicList extends Component {
	constructor(props) {
		super(props);

		this.toggleShowFavClick = this.toggleShowFavClick.bind(this);
		this.onAddToFavClick = this.onAddToFavClick.bind(this);
		this.onRemoveFavClick = this.onRemoveFavClick.bind(this);

		this.state = {
			isShowFav: false,
		};
	}

	componentDidMount() {
		this.props.dispatch(fetchComics());
	}

	toggleShowFavClick() {
		this.setState({
			isShowFav: !this.state.isShowFav
		});
	}

	onAddToFavClick(id) {
		this.props.dispatch(addFavComic(id))
	}

	onRemoveFavClick(id) {
		this.props.dispatch(removeFavComic(id))
	}

	render() {
		let favComics = []
		_forEach(this.props.favComicIDs, (fid) =>
			favComics.push(_find(this.props.comics, ['id', fid]))
		)
		return (
			<div>
	      <header className="site-header">
	        <h1 className="site-heading">Red Ant Comics</h1>
					<button
						className="favourites-toggle"
						onClick={this.toggleShowFavClick}
					>
						<span className="sr-only">My Facvourite Comics</span>
					</button>
	      </header>


	      <main className="site-content">
					{this.props.loading && <Loading/>}
					{this.props.comics &&
	          <ul id="comics-list" className="comics-list">
						{this.props.comics.map((comic, i) =>
							{
								const isFav = _filter(this.props.favComicIDs, (id) => id === comic.id).length > 0
								return (
									<li className="comic-item" key={i}>
										<div className="comic-card">
											<Comic comic={comic} />
											{isFav ? <div className="added-to-fav"><FontAwesomeIcon icon={faStar} /> Added to favourites</div>:
												<Button variant="secondary" className="button" onClick={() => this.onAddToFavClick(comic.id)}>
													Add to favourites
												</Button>
											}
										</div>
									</li>
								)
							}
						)}
	          </ul>
				}
				</main>

				<FavouritePanel
					favComics={favComics}
					isShowFav={this.state.isShowFav}
					handleToggleShowFavClick={this.toggleShowFavClick}
					handleRemoveFavClick={this.onRemoveFavClick}
				/>

			</div>
		);
	}
}

const mapStateToProps = state => ({
  comics: state.comics.comics,
  favComicIDs: state.comics.favComicIDs,
  loading: state.loading.loading,
});

export default connect(mapStateToProps)(ComicList);
