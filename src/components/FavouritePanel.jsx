import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTrashAlt} from '@fortawesome/free-solid-svg-icons'
import Comic from '../components/Comic';

const FavouritePanel = (props) => {
	return (
		<div id="favourites-panel" className={`favourites-panel ${props.isShowFav ? 'open' : ''}`}>
			<div className="favourites-header">
					<h2>Favourites</h2>
					<button className="close" onClick={props.handleToggleShowFavClick}></button>
			</div>

			<div className="favourites-content">
				<ul className="favourites-list">
				{props.favComics.map((comic, i) =>
					<li className="comic-item" key={i}>
						<div className="comic-card">
							<Comic comic={comic} />
							<Button variant="danger" onClick={() => props.handleRemoveFavClick(comic.id)}>
								<FontAwesomeIcon icon={faTrashAlt} /> Remove
							</Button>
						</div>
					</li>
				)}
				{props.favComics.length === 0 && <p className="text-center py-3">No Favourite Movies. Go add some.</p>}
				</ul>
			</div>
		</div>
	);
};

FavouritePanel.propTypes = {
	favComics: PropTypes.array,
	isShowFav: PropTypes.bool,
	handleToggleShowFavClick: PropTypes.func,
	handleRemoveFavClick: PropTypes.func,
};

export default FavouritePanel;
