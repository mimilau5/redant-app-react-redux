import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSpinner} from '@fortawesome/free-solid-svg-icons'

const Loading = () => (
	<div className="text-center py-3">
		<FontAwesomeIcon icon={faSpinner} spin size="5x" />
		<span className="sr-only">Loading...</span>
	</div>
);

export default Loading;
