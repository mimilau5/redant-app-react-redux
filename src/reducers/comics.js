import * as ActionTypes from '../constants/ActionTypes';
import _filter from 'lodash/filter';
import _concat from 'lodash/concat';

const initialState = {
  comics: [],
  favComicIDs: []
};

const comics = (state = initialState, action) => {
  switch(action.type) {
    case ActionTypes.FETCH_COMICS_SUCCESS:
      return {...state,
        comics: action.payload
      };
    case ActionTypes.FETCH_COMICS_FAILURE:
      return {...state,
        comics: []
      };
    case ActionTypes.ADD_FAV_COMIC:
      return {...state,
        favComicIDs: _concat(state.favComicIDs, action.payload)
      };
    case ActionTypes.REMOVE_FAV_COMIC:
      return {...state,
        favComicIDs: _filter(state.favComicIDs, (id => id !== action.payload))
      };
    default:
      return state;
  }
}

export default comics;
