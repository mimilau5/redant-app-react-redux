import { combineReducers } from "redux";
import comics from "./comics";
import loading from "./loading";

export default combineReducers({
  comics,
  loading
});
