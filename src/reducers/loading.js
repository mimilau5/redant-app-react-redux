import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
  loading: false,
};

const loading = (state = initialState, action) => {
  switch(action.type) {
    case ActionTypes.FETCH_COMICS_REQUEST:
    case ActionTypes.FETCH_COMICS_FAILURE:
      return Object.assign({}, state, {
        loading: true
      });
    case ActionTypes.FETCH_COMICS_SUCCESS:
      return Object.assign({}, state, {
        loading: false
      });
    default:
      return state;
  }
}

export default loading;
